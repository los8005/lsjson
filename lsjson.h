#ifndef LSJSON_H
#define LSJSON_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
using namespace std;

#define JSN_TRUE	"true"
#define JSN_FALSE	"false"
#define JSN_NULL	"null"

class LsJsonError
{
private:
	string what;
public:
	LsJsonError(string What,size_t Line)
	{
		stringstream ss(what);
		ss<<What<<" "<<Line;
	}
	operator const char*() const {return what.c_str();}
};

struct LsJsonObject
{
	/*
		Objects in the object
	*/
	map<string,LsJsonObject> objects;
	/*
		Value in object and array
	*/
	map<string,string> values;
	vector<string> unnamedval;
	/*
		Array in object and array
	*/
	map<string,vector<LsJsonObject> > arrays;
	vector<LsJsonObject> unnamedarr;
	/*
		Clear all data
	*/
	void Clear()
	{
		objects.clear();
		values.clear();
		unnamedval.clear();
		arrays.clear();
		unnamedarr.clear();
	}
};

class LsJson
{
private:
	LsJsonObject root;
	/*
		Get from source from start position string in str, return new position
	*/
	size_t ParseString(const string &source,const size_t start,string &str);
	/*
		Get from source from start  position number in num, return new position
	*/
	size_t ParseNumber(const string &source,const size_t start,string &num);
	/*
		Get from source from start position object in object
	*/
	size_t ParseObject(const string &source,const size_t start,LsJsonObject &object);
	/*
		Get from source from start position array in array
	*/
	size_t ParseArray(const string &source,const size_t start,vector<LsJsonObject> &array);
public:
	/*
		Parse JSON string
	*/
	void ParseJSON(const string &source)
	{
		root.Clear();
		ParseObject(source,0,root);
	}
	/*
		Get objects JSON
	*/
	LsJsonObject GetRoot() const {
		return root;
	}
	/*
		Operator >>
	*/
	friend istream &operator>>(istream &is,LsJson &lj);
};

#endif