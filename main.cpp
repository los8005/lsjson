#include <iostream>
using namespace std;

#include "lsjson.h"
/*
{"menu": {
		"id": "file",
		"value": "File",
		"count": 35.6e+4,
		"popup": {
			"menuitem": [
			{"value": "New", "onclick": "CreateNewDoc()"},
			{"value": "Open", "onclick": "OpenDoc()"},
			{"value": "Close", "onclick": "CloseDoc()"}
			]
		},
		"arr":[45.6e-3,"Test",[1,2]]
}}
*/
char example_json[] = "{\"menu\": {\"id\": \"file\",\"value\": \"File\", \"count\": 35.6e+4,\"popup\": {\"menuitem\": [{\"value\": \"New\", \"onclick\": \"CreateNewDoc()\"},{\"value\": \"Open\", \"onclick\": \"OpenDoc()\"},{\"value\": \"Close\", \"onclick\": \"CloseDoc()\"}]},\"arr\":[45.6e-3,\"Test\",[1,2]]}}";

void DumpObject(LsJsonObject &obj,string name)
{
	for(map<string,LsJsonObject>::iterator it = obj.objects.begin();it != obj.objects.end();it++)
	{
		cout<<name<<" have object "<<it->first<<endl;
		DumpObject(it->second,it->first);
		cout<<endl;
	}
	for(map<string,string>::iterator it = obj.values.begin();it != obj.values.end();it++)
	{
		cout<<name<<" have value "<<it->first<<" = "<<it->second<<endl;
	}
	for(vector<string>::iterator it = obj.unnamedval.begin();it != obj.unnamedval.end();it++)
	{
		cout<<name<<" have unnamedval "<<*it<<endl;
	}
	for(map<string,vector<LsJsonObject> >::iterator it = obj.arrays.begin();it != obj.arrays.end();it++)
	{
		cout<<name<<" have array "<<it->first<<endl;
		for(vector<LsJsonObject>::iterator itt = it->second.begin();itt != it->second.end();itt++)
		{
			DumpObject(*itt,name+" have array "+it->first+" item");
			cout<<endl;
		}
	}
	for(vector<LsJsonObject>::iterator it = obj.unnamedarr.begin();it != obj.unnamedarr.end();it++)
	{
		cout<<name<<" have unnamedarr"<<endl;
		DumpObject(*it,name+" have unnamed array");
		cout<<endl;
	}
}

int main(int argc,char *argv[])
{
	/*
		Main pluses
			- simply adding to project
			- simply using
			- minimal relation
			- minimal size
	*/
	LsJson lj;
	try
	{
		// It is possible
		// cin>>lj;
		// get data from stdin to lj
		lj.ParseJSON(example_json);
	}
	catch(LsJsonError e)
	{
		cout<<e<<endl;
	}

	LsJsonObject root = lj.GetRoot();

	cout<<"Object dump"<<endl;
	DumpObject(root,"root");
	cout<<"Object dump end"<<endl;

	cout<<root.objects["menu"].values["id"]<<endl;
	cout<<root.objects["menu"].values["value"]<<endl;
	
	cout<<root.objects["menu"].values["count"]<<endl;

	for(int i = 0;i<3;i++)
	{
		cout<<root.objects["menu"].objects["popup"].arrays["menuitem"][i].values["value"]<<endl;
		cout<<root.objects["menu"].objects["popup"].arrays["menuitem"][i].values["onclick"]<<endl;
	}

	vector<LsJsonObject> arr = root.objects["menu"].arrays["arr"];
	cout<<arr[0].unnamedval[0]<<endl;
	cout<<arr[1].unnamedval[0]<<endl;
	vector<LsJsonObject> inarr = arr[2].unnamedarr;
	cout<<inarr[0].unnamedval[0]<<endl;
	cout<<inarr[1].unnamedval[0]<<endl;

	int i;
	cin>>i;
	return 0;
}