#include "lsjson.h"

size_t LsJson::ParseString(const string &source,const size_t start,string &str)
{
	enum {S,F,E,A,B}ps = S;
	size_t pos = start;
	while(pos < source.length() && ps != F && ps != E)
	{
		char ch = source[pos++];
		if(ps == S)
		{
			if(ch == ' ')
				ps = S;
			else if(ch == '"')
				ps = A;
			else
				ps = E;
		}
		else if(ps == A)
		{
			if(ch != '"' && ch != '\\')
			{
				str += ch;
				ps = A;
			}
			else if(ch == '\\')
			{
				str += ch;
				ps = B;
			}
			else if(ch == '"')
			{
				ps = F;
			}
			else
			{
				ps = E;
			}
		}
		else if(ps == B)
		{
			str += ch;
			if(ch == ' ')
				ps = B;
			else
				ps = A;
		}
	}
	if(ps == E)
		throw LsJsonError("ParseString error on position",pos);
	return pos;
}

size_t LsJson::ParseNumber(const string &source,const size_t start,string &num)
{
	enum {S,A,B,C,D,G,K,F,E}ps = S;
	size_t pos = start;
	while(pos < source.length() && ps != F && ps != E)
	{
		char ch = source[pos++];
		if(ps == S)
		{
			if(ch == ' ')
			{
				ps = S;
			}
			else if(ch == '0')
			{
				num += ch;
				ps = C;
			}
			else if(ch >= '1' && ch <= '9')
			{
				num += ch;
				ps = A;
			}
			else if(ch == '-')
			{
				num += ch;
				ps = B;
			}
			else
			{
				ps = E;
			}
		}
		else if(ps == A)
		{
			if(ch == ' ')
			{
				ps = A;
			}
			else if(ch >= '0' && ch <= '9')
			{
				num += ch;
				ps = A;
			}
			else if(ch == '.')
			{
				num += ch;
				ps = D;
			}
			else if(ch == 'e' || ch == 'E')
			{
				num += ch;
				ps = G;
			}
			else
			{
				ps = F;
			}
		}
		else if(ps == B)
		{
			if(ch == ' ')
			{
				ps = B;
			}
			else if(ch == '0')
			{
				num += ch;
				ps = C;
			}
			else if(ch >= '1' && ch <= '9')
			{
				num += ch;
				ps = A;
			}
			else
			{
				ps = E;
			}
		}
		else if(ps == C)
		{
			if(ch == ' ')
			{
				ps = C;
			}
			else if(ch == '.')
			{
				num += ch;
				ps = D;
			}
			else if(ch == 'e' || ch == 'E')
			{
				num += ch;
				ps = G;
			}
			else
			{
				ps = F;
			}
		}
		else if(ps == D)
		{
			if(ch == ' ')
			{
				ps = D;
			}
			else if(ch >= '0' && ch <= '9')
			{
				num += ch;
				ps = D;
			}
			else if(ch == 'e' || ch == 'E')
			{
				num += ch;
				ps = G;
			}
			else
			{
				ps = F;
			}
		}
		else if(ps == G)
		{
			if(ch == ' ')
			{
				ps = G;
			}
			else if(ch == '+' || ch == '-' || (ch >='0' && ch <= '9'))
			{
				num += ch;
				ps = K;
			}
			else
			{
				ps = E;
			}
		}
		else if(ps == K)
		{
			if(ch == ' ')
			{
				ps = K;
			}
			else if(ch >= '0' && ch <= '9')
			{
				num += ch;
				ps = K;
			}
			else
			{
				ps = F;
			}
		}
	}
	if(ps == E)
		throw LsJsonError("ParseNumber error on position",pos);
	else if(ps == F)
		pos--;
	return pos;
}

size_t LsJson::ParseObject(const string &source,const size_t start,LsJsonObject &object)
{
	enum {S,A,B,C,D,F,E}ps = S;
	size_t pos = start;
	string parname;
	while(pos < source.length() && ps != F && ps != E)
	{
		if(ps == S)
		{
			if(source[pos] == ' ')
				ps = S;
			else if(source[pos] == '{')
				ps = A;
			else
				ps = E;
			pos++;
		}
		else if(ps == A)
		{
			pos = ParseString(source,pos,parname);
			ps = B;
		}
		else if(ps == B)
		{
			if(source[pos] == ' ')
				ps = B;
			else if(source[pos] == ':')
				ps = C;
			else
				ps = E;
			pos++;
		}
		else if(ps == C)
		{
			if(source[pos] == ' ')
			{
				ps = C;
				pos++;
			}
			else
			{
				if(source[pos] == '{')
				{
					pos = ParseObject(source,pos,object.objects[parname]);
				}
				else if(source[pos] == '[')
				{
					pos = ParseArray(source,pos,object.arrays[parname]);
				}
				else if(source.compare(pos,strlen(JSN_TRUE),JSN_TRUE) == 0)
				{
					object.values[parname] = JSN_TRUE;
					pos += strlen(JSN_TRUE);
				}
				else if(source.compare(pos,strlen(JSN_FALSE),JSN_FALSE) == 0)
				{
					object.values[parname] = JSN_FALSE;
					pos += strlen(JSN_FALSE);
				}
				else if(source.compare(pos,strlen(JSN_NULL),JSN_NULL) == 0)
				{
					object.values[parname] = JSN_NULL;
					pos += strlen(JSN_NULL);
				}
				else if(source[pos] == '"')
				{
					pos = ParseString(source,pos,object.values[parname]);
				}
				else
				{
					pos = ParseNumber(source,pos,object.values[parname]);
				}
				parname = "";
				ps = D;
			}
		}
		else if(ps == D)
		{
			if(source[pos] == ' ')
				ps = D;
			else if(source[pos] == ',')
				ps = A;
			else if(source[pos] == '}')
				ps = F;
			else
				ps = E;
			pos++;
		}
	}
	if(ps == E)
		throw LsJsonError("ParseObject error on position",pos);
	return pos;
}

size_t LsJson::ParseArray(const string &source,const size_t start,vector<LsJsonObject> &array)
{
	enum {S,A,B,F,E}ps = S;
	size_t pos = start;
	while(pos < source.length() && ps != F && ps != E)
	{
		if(ps == S)
		{
			if(source[pos] == ' ')
				ps = S;
			else if(source[pos] == '[')
				ps = A;
			else
				ps = E;
			pos++;
		}
		else if(ps == A)
		{
			if(source[pos] == ' ')
			{
				ps = A;
				pos++;
			}
			else
			{
				LsJsonObject obj;
				if(source[pos] == '{')
				{
					pos = ParseObject(source,pos,obj);
				}
				else if(source[pos] == '[')
				{
					pos = ParseArray(source,pos,obj.unnamedarr);
				}
				else if(source.compare(pos,strlen(JSN_TRUE),JSN_TRUE) == 0)
				{
					obj.unnamedval.push_back(JSN_TRUE);
					pos += strlen(JSN_TRUE);
				}
				else if(source.compare(pos,strlen(JSN_FALSE),JSN_FALSE) == 0)
				{
					obj.unnamedval.push_back(JSN_FALSE);
					pos += strlen(JSN_FALSE);
				}
				else if(source.compare(pos,strlen(JSN_NULL),JSN_NULL) == 0)
				{
					obj.unnamedval.push_back(JSN_NULL);
					pos += strlen(JSN_NULL);
				}
				else if(source[pos] == '"')
				{
					string vstr;
					pos = ParseString(source,pos,vstr);
					obj.unnamedval.push_back(vstr);
				}
				else
				{
					string vnum;
					pos = ParseNumber(source,pos,vnum);
					obj.unnamedval.push_back(vnum);
				}
				array.push_back(obj);
				ps = B;
			}
		}
		else if(ps == B)
		{
			if(source[pos] == ' ')
				ps = B;
			else if(source[pos] == ',')
				ps = A;
			else if(source[pos] == ']')
				ps = F;
			else
				ps = E;
			pos++;
		}
	}
	if(ps == E)
		throw LsJsonError("ParseArray error on position",pos);
	return pos;
}

istream &operator>>(istream &is,LsJson &lj)
{
	string val;
	getline(is,val);
	lj.ParseJSON(val);
	return is;
}